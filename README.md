The Goonies Remake
==================

History:
--------

The action takes place in the sleepy seaside town of Cauldron Point - down in
an area known as the Goon Docks. The Goonies are a group of local kids:

- **Mikey, Brand, Mouth, Chunk and Data**.

One day they find an old treasure map in **Mikey's** attic.

**Data** figures that this must be the fabulous hidden treasure of
the notorious local pirate, One-Eyed Willy.

The girls, **Andy and Stef**, join the other Goonies and the adventure begins.

But the secret underground tunnels they are exploring are actually the hideout
of the Fratelli Gang, and the Goonies are soon trapped!

The Goonies have a powerful ally on their side, however, in the form of
the huge and amiable Sloth.

When you play this game, you are Sloth! Can you help the Goonies find
the treasure and escape back to save the Goon Docks from demolition?

Credits:
--------

This remake was made for the Retro Remakes 2006 competition by Brain Games.
The game was made in the months June, July and August
of 2006 by the following people:

> **Coding:** Santi Ontanon, Jorrith Schaap, Patrick Lina, Nene Franz,
> David Fernandez
> 
> **Graphics:** David Fernandez, Miikka Poikela, Daedalus, Kelesisv, Nene Franz
> 
> **Music & SFX:** Jorrith Schaap
> 
> **Maps:** Jesus Perez Rosales, Mauricio Braga, Patrick Lina, Santi Ontanon
> 
> **Beta testing:** All of the mentioned above, Albert Beevendorp,
> Patrick Van Arkel

**Necessary dependencies**
--------------------------

You will need to have the following dependencies installed:

  * **SDL2, SDL2_image, SDL2_mixer**

On Debian GNU/Linux you can get all necessary packages by typing:

    # apt install g++ make libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev

Compilation:
------------

To build game, do from the source directory:

    $ make clean && make

Once the compilation has finished, execute the created binary:

    $ ./goonies

**License**
-----------

> Copyright (C) 2017 Carlos Donizete Froes [a.k.a coringao]
> 
> Use of this file is governed by a BSD 2-clause license
> that can be found in the LICENSE file.
> 
> Import of website The Goonies - 20th anniversary edition v.1.4 r1528 -
> [BRAIN GAMES](http://goonies.jorito.net)
