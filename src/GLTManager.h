/*
 * Copyright (C) 2017 Carlos Donizete Froes [a.k.a coringao]
 * Use of this file is governed by a BSD 2-clause license
 * that can be found in the LICENSE file.
 * Import of website The Goonies - 20th anniversary edition v.1.4 r1528 -
 * http://goonies.jorito.net
 * 
 * This file is part of The Gonnies Remake game.
 * 
 * File Name:	GLTManager.h
 * Update Date:	December/2017
 * 
 */

#ifndef _GLTILE_MANAGER
#define _GLTILE_MANAGER


class GLTManagerNode
{
    public:
		~GLTManagerNode();

        GLTile *m_tile;
        Symbol *m_name;
};


class GLTManager
{
    public:
        GLTManager();
        ~GLTManager();

        GLTile *get(char *name);
        GLTile *get(Symbol *name);

        GLTile *get_smooth(char *name);
        GLTile *get_smooth(Symbol *name);

		void cache(void);

    protected:

		char *remove_extension(char *filename);

		List<GLTManagerNode> *m_hash;
        List<GLTManagerNode> *m_hash_smooth;

};

#endif



