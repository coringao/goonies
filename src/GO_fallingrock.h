/*
 * Copyright (C) 2017 Carlos Donizete Froes [a.k.a coringao]
 * Use of this file is governed by a BSD 2-clause license
 * that can be found in the LICENSE file.
 * Import of website The Goonies - 20th anniversary edition v.1.4 r1528 -
 * http://goonies.jorito.net
 * 
 * This file is part of The Gonnies Remake game.
 * 
 * File Name:	GO_fallingrock.h
 * Update Date:	December/2017
 * 
 */

#ifndef _THE_GOONIES_OBJECT_FALLINGROCK
#define _THE_GOONIES_OBJECT_FALLINGROCK

class GO_fallingrock : public GO_enemy
{
    public:
        GO_fallingrock(int x, int y, int sfx_volume, int color);

        virtual bool cycle(VirtualController *k, GMap *map, int layer, class TheGoonies *game, GLTManager *GLTM, SFXManager *SFXM);
        virtual void draw(GLTManager *GLTM);

        virtual int enemy_hit(void);

        virtual bool is_a(Symbol *c);
        virtual bool is_a(char *c);

    protected:
        int m_color;  // 0 : normal, 1: blue, 2: green

};

#endif

