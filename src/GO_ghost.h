/*
 * Copyright (C) 2017 Carlos Donizete Froes [a.k.a coringao]
 * Use of this file is governed by a BSD 2-clause license
 * that can be found in the LICENSE file.
 * Import of website The Goonies - 20th anniversary edition v.1.4 r1528 -
 * http://goonies.jorito.net
 * 
 * This file is part of The Gonnies Remake game.
 * 
 * File Name:	GO_ghost.h
 * Update Date:	December/2017
 * 
 */

#ifndef _THE_GOONIES_OBJECT_GHOST
#define _THE_GOONIES_OBJECT_GHOST

class GO_ghost : public GO_enemy
{
    public:
        GO_ghost(int x, int y, int sfx_volume);

        virtual bool cycle(VirtualController *k, GMap *map, int layer, class TheGoonies *game, GLTManager *GLTM, SFXManager *SFXM);
        virtual void draw(GLTManager *GLTM);

        virtual bool player_hit(int *experience, int *score);
        virtual int enemy_hit(void);

        virtual bool is_a(Symbol *c);
        virtual bool is_a(char *c);

    protected:
        float m_original_x, m_original_y;
        int m_energy;
        int m_movement_state;
        int m_hit_timmer;
        float m_destination_x, m_destination_y;
        float m_radius;
};

#endif

