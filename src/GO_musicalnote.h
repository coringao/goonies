/*
 * Copyright (C) 2017 Carlos Donizete Froes [a.k.a coringao]
 * Use of this file is governed by a BSD 2-clause license
 * that can be found in the LICENSE file.
 * Import of website The Goonies - 20th anniversary edition v.1.4 r1528 -
 * http://goonies.jorito.net
 * 
 * This file is part of The Gonnies Remake game.
 * 
 * File Name:	GO_musicalnote.h
 * Update Date:	December/2017
 * 
 */

#ifndef _THE_GOONIES_OBJECT_MUSICALNOTE
#define _THE_GOONIES_OBJECT_MUSICALNOTE

class GO_musicalnote : public GO_enemy
{
    public:
        GO_musicalnote(int x, int y, int sfx_volume, float speedx, float speedy);

        virtual bool cycle(VirtualController *k, GMap *map, int layer, class TheGoonies *game, GLTManager *GLTM, SFXManager *SFXM);
        virtual void draw(GLTManager *GLTM);

        virtual int enemy_hit(void);

        virtual bool is_a(Symbol *c);
        virtual bool is_a(char *c);

    protected:
        float m_speedx, m_speedy;
};

#endif

