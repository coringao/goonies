/*
 * Copyright (C) 2017 Carlos Donizete Froes [a.k.a coringao]
 * Use of this file is governed by a BSD 2-clause license
 * that can be found in the LICENSE file.
 * Import of website The Goonies - 20th anniversary edition v.1.4 r1528 -
 * http://goonies.jorito.net
 * 
 * This file is part of The Gonnies Remake game.
 * 
 * File Name:	GO_pipe_water.h
 * Update Date:	December/2017
 * 
 */

#ifndef _THE_GOONIES_OBJECT_PIPE_WATER
#define _THE_GOONIES_OBJECT_PIPE_WATER

class GO_pipe_water : public GO_enemy
{
    public:
        GO_pipe_water(int x, int y, int sfx_volume, int facing);

        virtual bool cycle(VirtualController *k, GMap *map, int layer, class TheGoonies *game, GLTManager *GLTM, SFXManager *SFXM);
        virtual void draw(GLTManager *GLTM);

        virtual bool player_hit(int *experience, int *score);
        virtual int enemy_hit(void);

        virtual bool is_a(Symbol *c);
        virtual bool is_a(char *c);


    protected:
        int m_facing;
        int m_wait_cycles;
        int m_pipe_channel;
};

#endif

