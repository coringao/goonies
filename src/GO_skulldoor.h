/*
 * Copyright (C) 2017 Carlos Donizete Froes [a.k.a coringao]
 * Use of this file is governed by a BSD 2-clause license
 * that can be found in the LICENSE file.
 * Import of website The Goonies - 20th anniversary edition v.1.4 r1528 -
 * http://goonies.jorito.net
 * 
 * This file is part of The Gonnies Remake game.
 * 
 * File Name:	GO_skulldoor.h
 * Update Date:	December/2017
 * 
 */

#ifndef _THE_GOONIES_OBJECT_SKULLDOOR
#define _THE_GOONIES_OBJECT_SKULLDOOR

class GO_skulldoor : public GObject
{
    public:
        GO_skulldoor(int x, int y, int sfx_volume);
        GO_skulldoor(int x, int y, int sfx_volume, int dx, int dy, int dd);

        virtual bool cycle(VirtualController *k, GMap *map, int layer, class TheGoonies *game, GLTManager *GLTM, SFXManager *SFXM);
        virtual void draw(GLTManager *GLTM);

        virtual bool is_a(Symbol *c);
        virtual bool is_a(char *c);

        int get_destination_x(void)
        {
            return m_destination_x;
        };
        int get_destination_y(void)
        {
            return m_destination_y;
        };
        int get_destination_door(void)
        {
            return m_destination_door;
        };

    protected:
        int m_destination_x, m_destination_y, m_destination_door;
		int m_opening_counter;
		int m_glow_counter;
};

#endif

