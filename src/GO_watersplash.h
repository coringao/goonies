/*
 * Copyright (C) 2017 Carlos Donizete Froes [a.k.a coringao]
 * Use of this file is governed by a BSD 2-clause license
 * that can be found in the LICENSE file.
 * Import of website The Goonies - 20th anniversary edition v.1.4 r1528 -
 * http://goonies.jorito.net
 * 
 * This file is part of The Gonnies Remake game.
 * 
 * File Name:	GO_watersplash.h
 * Update Date:	December/2017
 * 
 */

#ifndef _THE_GOONIES_OBJECT_WATERSPLASH
#define _THE_GOONIES_OBJECT_WATERSPLASH

class GO_watersplash : public GObject
{
    public:
        GO_watersplash(int x, int y, float speed_x, float speed_y, int sfx_volume);

        virtual bool cycle(VirtualController *k, GMap *map, int layer, class TheGoonies *game, GLTManager *GLTM, SFXManager *SFXM);
        virtual void draw(GLTManager *GLTM);

        virtual bool is_a(Symbol *c);
        virtual bool is_a(char *c);


    protected:

        int m_dx, m_dy;
        float m_speed_x, m_speed_y;
};

#endif

