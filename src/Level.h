/*
 * Copyright (C) 2017 Carlos Donizete Froes [a.k.a coringao]
 * Use of this file is governed by a BSD 2-clause license
 * that can be found in the LICENSE file.
 * Import of website The Goonies - 20th anniversary edition v.1.4 r1528 -
 * http://goonies.jorito.net
 * 
 * This file is part of The Gonnies Remake game.
 * 
 * File Name:	Level.h
 * Update Date:	December/2017
 * 
 */

#ifndef _GONNIES_LEVEL
#define _GOONIES_LEVEL

class GLevel {
public:
	GLevel();
	~GLevel();

	void setName(char *name);
	void setPassword(char *pwd);
	char *getName(void);
	char *getPassword(void);
	
protected:
	Symbol *m_name;
	Symbol *m_password;

};


#endif
