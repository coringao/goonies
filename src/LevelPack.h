/*
 * Copyright (C) 2017 Carlos Donizete Froes [a.k.a coringao]
 * Use of this file is governed by a BSD 2-clause license
 * that can be found in the LICENSE file.
 * Import of website The Goonies - 20th anniversary edition v.1.4 r1528 -
 * http://goonies.jorito.net
 * 
 * This file is part of The Gonnies Remake game.
 * 
 * File Name:	LevelPack.h
 * Update Date:	December/2017
 * 
 */

#ifndef _GONNIES_LEVELPACK
#define _GOONIES_LEVELPACK

class GLevelPack {
public:
	GLevelPack();
	~GLevelPack();

	void addNewLevel(GLevel *level);
	void deleteLevel(GLevel *level);
	int getNLevels(void);
	int getLevelPosition(GLevel *level);
	GLevel *getLevel(int n);
	GLevel *getLevel(char *name);
	void setLevel(int n,GLevel *level);
	
protected:

	List<GLevel> m_levels;
};


#endif
