/*
 * Copyright (C) 2017 Carlos Donizete Froes [a.k.a coringao]
 * Use of this file is governed by a BSD 2-clause license
 * that can be found in the LICENSE file.
 * Import of website The Goonies - 20th anniversary edition v.1.4 r1528 -
 * http://goonies.jorito.net
 * 
 * This file is part of The Gonnies Remake game.
 * 
 * File Name:	PlacedGLTile.h
 * Update Date:	December/2017
 * 
 */

#ifndef __BRAIN_PLACED_GL_TILE
#define __BRAIN_PLACED_GL_TILE


class CPlacedGLTile
{
        friend class CTrack;
    public:
        CPlacedGLTile(float x, float y, float z, float angle, GLTile *t);
        ~CPlacedGLTile();

        void set_tile(GLTile *t)
        {
            tile = t;
        };
        void set_color(float pr, float pg, float pb, float pa)
        {
            r = pr;
            g = pg;
            b = pb;
            a = pa;
            different_color = true;
        };
        void set_nocolor(void)
        {
            different_color = false;
        };

        void draw(void);
        void draw(float dx, float dy, float dz, float angle, float zoom);

        C2DCMC *get_cmc(void)
        {
            return tile->get_cmc();
        };

        float x, y, z, angle, zoom;
        bool different_color;
        float r, g, b, a;

        GLTile *tile;
};


#endif
