/*
 * Copyright (C) 2017 Carlos Donizete Froes [a.k.a coringao]
 * Use of this file is governed by a BSD 2-clause license
 * that can be found in the LICENSE file.
 * Import of website The Goonies - 20th anniversary edition v.1.4 r1528 -
 * http://goonies.jorito.net
 * 
 * This file is part of The Gonnies Remake game.
 * 
 * File Name:	Vector.h
 * Update Date:	December/2017
 * 
 */

#ifndef __AW_VECTOR
#define __AW_VECTOR

class Vector
{
    public:
        Vector();
        Vector(double nx, double ny, double nz);
        Vector(const Vector &v);
        Vector(FILE *fp)
        {
            x = y = z = 0;
            load(fp);
        }

        Vector operator+(const Vector &v);
        Vector operator-(const Vector &v);
        Vector operator-(void);

        Vector operator^(const Vector &v);
        double operator*(const Vector &v);
        Vector operator*(double ctnt);

        Vector operator/(double ctnt);

        bool operator==(const Vector &v);
        bool operator!=(const Vector &v);

        bool zero();

        double norma(void);
        double normalize(void);

        bool load(FILE *fp);

        bool save(FILE *fp);

        double x, y, z;
};

#endif
