/*
 * Copyright (C) 2017 Carlos Donizete Froes [a.k.a coringao]
 * Use of this file is governed by a BSD 2-clause license
 * that can be found in the LICENSE file.
 * Import of website The Goonies - 20th anniversary edition v.1.4 r1528 -
 * http://goonies.jorito.net
 * 
 * This file is part of The Gonnies Remake game.
 * 
 * File Name:	debug.h
 * Update Date:	December/2017
 * 
 */

#ifndef _F1SPIRIT_DEBUG
#define _F1SPIRIT_DEBUG

#define __DEBUG_MESSAGES

void output_debug_message(const char *fmt, ...);
void close_debug_messages(void);


#endif
