/*
 * Copyright (C) 2017 Carlos Donizete Froes [a.k.a coringao]
 * Use of this file is governed by a BSD 2-clause license
 * that can be found in the LICENSE file.
 * Import of website The Goonies - 20th anniversary edition v.1.4 r1528 -
 * http://goonies.jorito.net
 * 
 * This file is part of The Gonnies Remake game.
 * 
 * File Name:	font_extractor.h
 * Update Date:	December/2017
 * 
 */

#ifndef __FONT_EXTRACTOR
#define __FONT_EXTRACTOR

void font_release(void);

void font_extract(char *prefix, char *graphic_file, int nc, char *characters);
void font_preview(char *prefix);

void font_print(char *font, char *text);
void font_print(char *font, char *text, int x_offset,float alpha);
void font_print(int x, int y, int z, float angle, float zoom, char *font, char *text);
void font_print(int x, int y, int z, float angle, float zoom, char *font, char *text, int x_offset);
void font_print(int x, int y, int z, float angle, float zoom, float alpha, char *font, char *text, int x_offset);

void font_print_c(char *font, char *text);
void font_print_c(char *font, char *text, int x_offset,float alpha);
void font_print_c(int x, int y, int z, float angle, float zoom, char *font, char *text);
void font_print_c(int x, int y, int z, float angle, float zoom, char *font, char *text, int x_offset);
void font_print_c(int x, int y, int z, float angle, float zoom, float alpha, char *font, char *text, int x_offset);

void font_box_c(char *font, char *text, int *x, int *y, int *dx, int *dy);
void font_box_c(char *font, char *text, int *x, int *y, int *dx, int *dy, int x_offset);

void font_print(char *prefix, char *text, int x_offset, bool y_center);
void font_print(int x, int y, int z, float angle, float scale, char *font, char *text, int x_offset, bool y_center);

#endif

