/*
 * Copyright (C) 2017 Carlos Donizete Froes [a.k.a coringao]
 * Use of this file is governed by a BSD 2-clause license
 * that can be found in the LICENSE file.
 * Import of website The Goonies - 20th anniversary edition v.1.4 r1528 -
 * http://goonies.jorito.net
 * 
 * This file is part of The Gonnies Remake game.
 * 
 * File Name:	keyboardstate.h
 * Update Date:	December/2017
 * 
 */

#ifndef _BRAIN_KEYBOARDSTATE
#define _BRAIN_KEYBOARDSTATE

class KEYBOARDSTATE
{
    public:
        KEYBOARDSTATE();
        ~KEYBOARDSTATE();

        void copy(KEYBOARDSTATE *lvk);

        void cycle(void);

        bool save(FILE *fp);
        bool load(FILE *fp);

		bool key_press(int key);			/* returns if a key has been pressed				*/
											/* it returns true, just when the key is pressed	*/
											/* and keeps returning yes each 2 cycles after 20	*/  

        int n_joysticks;
        SDL_Joystick **joysticks;

        int joystick_0_pos, joystick_size; /* start of the first joystick and joystick size */
        int LVK_ID;

        int k_size;       /* number of inputs */
        unsigned char *old_keyboard;
        unsigned char *keyboard;
		int *time_pressed;					// amount of cycles that the key has been pressed
        List<SDL_Keysym> keyevents;
};

#endif
