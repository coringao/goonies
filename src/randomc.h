/*
 * Copyright (C) 2017 Carlos Donizete Froes [a.k.a coringao]
 * Use of this file is governed by a BSD 2-clause license
 * that can be found in the LICENSE file.
 * Import of website The Goonies - 20th anniversary edition v.1.4 r1528 -
 * http://goonies.jorito.net
 * 
 * This file is part of The Gonnies Remake game.
 * 
 * File Name:	randomc.h
 * Update Date:	December/2017
 * 
 */

#ifndef RANDOMC_H
#define RANDOMC_H

#include <math.h>
#include <assert.h>
#include <stdio.h>


class TRanrotBGenerator
{              // encapsulate random number generator
        enum constants {                     // define parameters
            KK = 17, JJ = 10, R1 = 13, R2 = 9};
    public:
        void RandomInit(int seed);      // initialization
        int IRandom(int min, int max);       // get integer random number in desired interval
        double Random();                     // get floating point random number
        TRanrotBGenerator(int seed);    // constructor
    protected:
        int p1, p2;                          // indexes into buffer
        unsigned int randbuffer[KK];        // history buffer
        unsigned int randbufcopy[KK*2];     // used for self-test
        enum TArch {TA_LITTLE_ENDIAN, TA_BIG_ENDIAN, TA_NON_IEEE};
        TArch Architecture;                  // conversion to float depends on computer architecture
};


#endif

