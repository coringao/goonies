/*
 * Copyright (C) 2017 Carlos Donizete Froes [a.k.a coringao]
 * Use of this file is governed by a BSD 2-clause license
 * that can be found in the LICENSE file.
 * Import of website The Goonies - 20th anniversary edition v.1.4 r1528 -
 * http://goonies.jorito.net
 * 
 * This file is part of The Gonnies Remake game.
 * 
 * File Name:	sound.h
 * Update Date:	December/2017
 * 
 */

#ifndef __BRAIN_SDL_SOUND
#define __BRAIN_SDL_SOUND

bool Sound_initialization(void);
int Sound_initialization(int nc, int nrc);
void Sound_release(void);

/*
int Sound_play(Mix_Chunk s);
int Sound_play(Mix_Chunk *s, int volume);
int Sound_play(Mix_Chunk *s, int volume, int distance);
int Sound_play(Mix_Chunk *s, int volume, int angle, int distance);

int Sound_play_continuous(Mix_Chunk *s);
int Sound_play_continuous(Mix_Chunk *s, int volume);
int Sound_play_continuous(Mix_Chunk *s, int volume, int distance);
int Sound_play_continuous(Mix_Chunk *s, int volume, int angle, int distance);

int Sound_play_ch(Mix_Chunk *s, int channel, int volume);
int Sound_play_ch(Mix_Chunk *s, int channel, int volume, int distance);
int Sound_play_ch(Mix_Chunk *s, int channel, int volume, int angle, int distance);
*/

#endif

